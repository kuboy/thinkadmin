/*
SQLyog Ultimate v11.28 (64 bit)
MySQL - 5.5.40 : Database - thinkadmin
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`thinkadmin` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `thinkadmin`;

/*Table structure for table `think_auth_group` */

DROP TABLE IF EXISTS `think_auth_group`;

CREATE TABLE `think_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户组表';

/*Data for the table `think_auth_group` */

insert  into `think_auth_group`(`id`,`title`,`status`,`rules`) values (1,'管理组',1,'1,2,11,13,12,14,17,18,43,44,45,46,32,36,37,38,39,16,40,41,42'),(2,'新闻一组',1,'6,8'),(3,'新闻二组',1,''),(4,'运维一组',1,'1,4'),(5,'运维二组',1,'1,5');

/*Table structure for table `think_auth_group_access` */

DROP TABLE IF EXISTS `think_auth_group_access`;

CREATE TABLE `think_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

/*Data for the table `think_auth_group_access` */

insert  into `think_auth_group_access`(`uid`,`group_id`) values (1,1),(1,2),(2,1),(2,2);

/*Table structure for table `think_auth_rule` */

DROP TABLE IF EXISTS `think_auth_rule`;

CREATE TABLE `think_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` mediumint(9) DEFAULT '0',
  `pth` varchar(30) DEFAULT '' COMMENT '记录菜单的层级关系',
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `sort` tinyint(4) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:显示 0:隐藏',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='规则表';

/*Data for the table `think_auth_rule` */

INSERT INTO `think_auth_rule` VALUES ('1', '0', '0,1', '', '控制台', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('2', '1', '0,1,2', 'admin.index.welcome', '欢迎', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('3', '1', '0,1,3', '', '缓存', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('4', '1', '0,1,4', '', '报表', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('5', '1', '0,1,5', '', '备份', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('6', '0', '0,6', '', '站务管理', '1', '1', '1', '');
INSERT INTO `think_auth_rule` VALUES ('7', '6', '0,6,7', '', '网站菜单', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('8', '6', '0,6,8', '', '新闻', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('9', '6', '0,6,9', '', '广告', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('10', '6', '0,6,10', '', '友链', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('11', '0', '0,11', '', '会员服务', '1', '2', '1', '');
INSERT INTO `think_auth_rule` VALUES ('12', '11', '0,11,12', '', '用户管理', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('13', '11', '0,11,13', '', '短信管理', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('14', '0', '0,14', '', '系统', '1', '3', '1', '');
INSERT INTO `think_auth_rule` VALUES ('15', '14', '0,14,15', '', '设置', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('16', '14', '0,14,16', 'admin.system.menuList', '菜单', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('17', '14', '0,14,17', '', '权限', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('18', '17', '0,14,17,18', 'admin.member.index', '管理员', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('19', '14', '0,14,19', '', '日志', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('20', '1', '0,1,20', '', '运维', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('21', '0', '0,21', '', '其他', '1', '4', '1', '');
INSERT INTO `think_auth_rule` VALUES ('22', '21', '0,21,22', '', '游戏', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('23', '22', '0,21,22,23', '', '小转盘', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('24', '22', '0,21,22,24', '', '大转盘', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('36', '32', '0,14,17,32,36', 'admin.group.add', '增加角色', '1', '0', '0', '');
INSERT INTO `think_auth_rule` VALUES ('35', '21', '0,21,35', '', '备忘录', '1', '1', '1', '');
INSERT INTO `think_auth_rule` VALUES ('32', '17', '0,14,17,32', 'admin.group.index', '角色', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('33', '21', '0,21,33', '', '工具', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('34', '33', '0,21,33,34', '', '缓存清理', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('37', '32', '0,14,17,32,37', 'admin.group.edit', '编辑角色', '1', '1', '0', '');
INSERT INTO `think_auth_rule` VALUES ('38', '32', '0,14,17,32,38', '', '禁用角色', '1', '2', '0', '');
INSERT INTO `think_auth_rule` VALUES ('39', '32', '0,14,17,32,39', 'admin.group.grant', '授权角色', '1', '3', '0', '');
INSERT INTO `think_auth_rule` VALUES ('40', '16', '0,14,16,40', 'admin.system.add', '添加菜单', '1', '0', '0', '');
INSERT INTO `think_auth_rule` VALUES ('41', '16', '0,14,16,41', 'admin.system.edit', '编辑菜单', '1', '1', '0', '');
INSERT INTO `think_auth_rule` VALUES ('42', '16', '0,14,16,42', 'admin.system.delete', '删除菜单', '1', '2', '0', '');
INSERT INTO `think_auth_rule` VALUES ('43', '18', '0,14,17,18,43', 'admin.member.add', '管理员添加', '1', '0', '0', '');
INSERT INTO `think_auth_rule` VALUES ('44', '18', '0,14,17,18,44', 'admin.member.edit', '管理员编辑', '1', '1', '0', '');
INSERT INTO `think_auth_rule` VALUES ('45', '18', '0,14,17,18,45', 'admin.index.editPassword', '管理员改密', '1', '2', '0', '');
INSERT INTO `think_auth_rule` VALUES ('46', '18', '0,14,17,18,46', 'admin.member.grant', '管理员授权', '1', '3', '0', '');
INSERT INTO `think_auth_rule` VALUES ('47', '8', '0,6,8,47', 'admin.category.index', '新闻分类', '1', '1', '1', '');
INSERT INTO `think_auth_rule` VALUES ('48', '8', '0,6,8,48', 'admin.news.index', '新闻管理', '1', '0', '1', '');
INSERT INTO `think_auth_rule` VALUES ('49', '47', '0,6,8,47,49', 'admin.category.add', '添加分类', '1', '0', '0', '');
INSERT INTO `think_auth_rule` VALUES ('50', '47', '0,6,8,47,50', 'admin.category.edit', '编辑分类', '1', '1', '0', '');
INSERT INTO `think_auth_rule` VALUES ('51', '47', '0,6,8,47,51', 'admin.category.delete', '删除分类', '1', '2', '0', '');
INSERT INTO `think_auth_rule` VALUES ('52', '48', '0,6,8,48,52', 'admin.news.add', '添加新闻', '1', '0', '0', '');
INSERT INTO `think_auth_rule` VALUES ('53', '48', '0,6,8,48,53', 'admin.news.edit', '编辑新闻', '1', '1', '0', '');
INSERT INTO `think_auth_rule` VALUES ('54', '48', '0,6,8,48,54', 'admin.news.delete', '删除新闻', '1', '2', '0', '');
INSERT INTO `think_auth_rule` VALUES ('55', '48', '0,6,8,48,55', 'admin.news.ueditor', '初始化ueditor', '1', '3', '0', '');
INSERT INTO `think_auth_rule` VALUES ('56', '48', '0,6,8,48,56', 'admin.news.upload', '图片上传', '1', '4', '0', '');

/*Table structure for table `think_auth_user` */

DROP TABLE IF EXISTS `think_auth_user`;

CREATE TABLE `think_auth_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `phone` char(11) NOT NULL COMMENT '操作管理员的手机号码',
  `salt` varchar(8) NOT NULL DEFAULT '' COMMENT '随机因子',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1:启用 0:禁止',
  `super` tinyint(1) DEFAULT '0' COMMENT '1:超级管理员 0:普通管理员',
  `remark` varchar(255) NOT NULL COMMENT '备注说明',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_location` varchar(100) NOT NULL DEFAULT '0' COMMENT '最后登录位置',
  PRIMARY KEY (`id`),
  KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='AUTH-权限管理-后台管理员用户表';

/*Data for the table `think_auth_user` */

insert  into `think_auth_user`(`id`,`username`,`password`,`phone`,`salt`,`status`,`super`,`remark`,`create_at`,`last_login_time`,`last_login_ip`,`last_location`) values (1,'admin','1fd94b7170dc92c3a7bb642446fc5d30','','AQ2re5',1,1,'超级管理员','2018-04-12 20:27:20','0000-00-00 00:00:00','0','0'),(2,'guest','b62581104c99079c5b14ec51979b1b59','','voBMjD',1,0,'','2018-05-15 12:39:29','0000-00-00 00:00:00','0','0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

CREATE TABLE `think_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '标题',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(500) NOT NULL DEFAULT '' COMMENT '描述/简介',
  `content` text COMMENT '内容',
  `status` tinyint(4) DEFAULT '1' COMMENT '1:正常 0:删除',
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `think_news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `pid_fields` varchar(255) DEFAULT '' COMMENT '节点关系',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `status` tinyint(4) DEFAULT '1' COMMENT '1:显示 0:删除 2:隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;