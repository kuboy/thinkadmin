<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

//如果你的域名后缀比较特殊，例如是com.cn或者net.cn 之类的域名，需要在应用配置文件app.php中配置：
//'url_domain_root'	=>	'91-t.com.cn'


//Route::rule('login', 'adminer/login/index')->name('admin.login');
// 后台域名绑定
$backendDomain = Env::get('domain.admin');
// 前台域名绑定
$frontendDomain = Env::get('domain.front');



Route::domain($backendDomain, function () {
    Route::rule('/', 'index/index');
    Route::rule('login', 'adminer/login/index')->name('admin.login');
    Route::rule('loginout', 'notAuth/loginout')->name('admin.loginout');
    Route::rule('editpassword', 'index/editpassword')->name('admin.editpassword');

    Route::group('index', function() {
        Route::rule('index', 'index/index')->name('admin.index');
        Route::rule('diy', 'index/diy')->name('admin.index.diy');
        Route::get('welcome', 'index/welcome')->name('admin.index.welcome');
    });

    Route::group('news', function () {
        Route::rule('index', 'news/index')->name('admin.news.index');
        Route::rule('add', 'news/add')->name('admin.news.add');
        Route::rule('edit', 'news/edit')->name('admin.news.edit');
        Route::rule('delete', 'news/delete')->name('admin.news.delete');
        Route::rule('upload', 'news/upload')->name('admin.news.upload');
        Route::rule('ueditor', 'news/ueditor')->name('admin.news.ueditor');
    });

    Route::group('system', function () {
        Route::rule('list', 'system/menulist')->name('admin.system.list');
        Route::rule('add', 'system/addMenu')->name('admin.system.add');
        Route::rule('edit', 'system/editMenu')->name('admin.system.edit');
        Route::rule('delete', 'system/delMenu')->name('admin.system.delete');
    });

    Route::group('member', function () {
        Route::rule('index', 'member/index')->name('admin.member.index');
        Route::rule('add', 'member/add')->name('admin.member.add');
        Route::rule('edit', 'member/edit')->name('admin.member.edit');
        Route::rule('delete', 'member/delete')->name('admin.member.delete');
    });

    Route::group('category', function () {
        Route::rule('index', 'category/index')->name('admin.category.index');
        Route::rule('add', 'category/add')->name('admin.category.add');
        Route::rule('edit', 'category/edit')->name('admin.category.edit');
        Route::rule('delete', 'category/delete')->name('admin.category.delete');
    });

    Route::group('group', function () {
        Route::rule('index', 'group/index')->name('admin.group.index');
        Route::rule('add', 'group/add')->name('admin.group.add');
        Route::rule('edit', 'group/edit')->name('admin.group.edit');
        Route::rule('grant', 'group/grantAuth')->name('admin.group.grant');
    });
})->bind('adminer');

Route::domain($frontendDomain, function () {
    Route::rule('/', 'index/index')->name('front.index');
})->bind('index');

// 公共路由
Route::group('util', function() {
    Route::rule('captcha', 'util/tool/captcha')->name('util.captcha');
});
