ThinkAdmin v1.1
===============
##版本记录  
1.0         
+ 后台菜单管理
+ 权限管理
+ 新闻管理

1.1         
+ 全路由处理
+ 前后台自定义域名

基于ThinkPHP5.1 感谢ThinkPHP官方的努力祝ThinkPHP越来越好,感谢LayUI让写html变得更加简单
本框架为轻量级后台管理系统,只提供最基础功能,更多功能需根据实际业务由个人进一步开发

需要注意的两个基类:

 + Adminbase 严格的权限检查
 + NotAuth 需要登陆但是不需要权限检查
 
实际开发中有许多应用场景需要权限检查,也有特殊的场景只登陆不要做权限检查,这两个基类可以满足需要
 
> ThinkPHP5的运行环境要求PHP5.6以上。

## 注意事项

手动导入thinkadmin.sql。  
重命名.env.example为.env。  
Nginx配置样例 
```$xslt
server {
        listen        80;
        server_name  thinkadmin.91-t.com front.91-t.com;
        root   "/app/thinkadmin/public";
        location / {
            index index.php index.html error/index.html;
            autoindex  off;
            if (!-e $request_filename) {
                rewrite  ^(.*)$  /index.php?s=/$1  last;
                break;
            }
        }
        location ~ \.php(.*)$ {
            fastcgi_pass   127.0.0.1:9000;
            fastcgi_index  index.php;
            fastcgi_split_path_info  ^((?U).+\.php)(/?.+)$;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            fastcgi_param  PATH_INFO  $fastcgi_path_info;
            fastcgi_param  PATH_TRANSLATED  $document_root$fastcgi_path_info;
            include        fastcgi_params;
        }
}
```  
composer update 一下,需要本机安装composer,自行百度如何安装。  

本项目提供了一个操作redis的类库tp5redis,可在[这里查看如何操作](https://github.com/xiucaiwu/tp5redis "")

## 在线体验
[点我爽一下](http://front.91-t.com/ "爽一下")  
账号: guest  
密码: 123456  

## 界面预览

![界面预览](https://gitee.com/bullet/thinkadmin/raw/master/screenshots/20180516182602.png "截图1")
![界面预览](https://gitee.com/bullet/thinkadmin/raw/master/screenshots/20180516182636.png "截图2")
![界面预览](https://gitee.com/bullet/thinkadmin/raw/master/screenshots/20180516182712.png "截图3")

## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。
LayUI遵循MIT开源协议发布，并提供免费使用。
ThinkAdmin遵循MIT开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。
