<?php
/**
 * Created by PhpStorm.
 * User: Louis
 * Date: 2019/12/18
 * Time: 16:55
 */

namespace app\adminer\controller;

use think\Db;
use PHPTool\SelectTree;

class Category extends Adminbase
{
    public function index() {
        if(!$this->request->isAjax()) {
            $count = Db::name('news_category')->count();
            $this->assign('total', $count);
            return $this->fetch('categorylist');
        } else {
            $page = input('get.page', 1);
            $limit = input('get.limit', 10);
            $offset = ($page - 1) * $limit;

            $count = Db::name('news_category')->count();
            $res = Db::name('news_category')
                ->order('id', 'desc')
                ->limit($offset, $limit)
                ->select();

            $data = [
                'count' => $count,
                'data'  => $res,
                'code'  => 0,
                'msg'   => 'success',
            ];
            echo json_encode($data);
            exit;
        }

    }

    public function add() {
        if($this->request->isGet()) {
            $pid = input('get.pid');
            $res = Db::name('news_category')->field('id, pid parentid, title name')->select();

            $st = new SelectTree($res);
            $tree = $st->getArray(0,0, ' ');

            $this->assign('tree', $tree);
            $this->assign('pid', $pid);

            return $this->fetch('categoryadd');
        } else {
            $pid = input('post.category', 0);
            $title = input('post.title');
            if(empty($title)) {
                $this->resultData('$_220');
            }
            $res = Db::name('news_category')->field('pid_fields')->where(['id' => $pid])->find();
            $pid_fields = $res ? $res['pid_fields'] : '0';
            $insertData = [
                'pid' => $pid,
                'title' => $title,
                'status' => input('post.status', 0)
            ];

            try{
                $id = Db::name('news_category')->insertGetId($insertData);
                $pid_fields = $pid_fields . ',' . $id;
                Db::name('news_category')->where(['id'=>$id])->update(['pid_fields'=>$pid_fields]);
                $this->resultData('$_0');
            } catch (\Exception $e) {
                $this->resultData('$_101');
            }
        }
    }

    public function edit() {
        if($this->request->isGet()) {
            $id = input('get.id');
            $list = Db::name('news_category')->where(['id'=>$id])->find();
            $res = Db::name('news_category')->field('id, pid parentid, title name')->select();

            $st = new SelectTree($res);
            $tree = $st->getArray(0,0, ' ');

            $this->assign('tree', $tree);
            $this->assign('id', $id);
            $this->assign('list', $list);

            return $this->fetch('categoryedit');
        } else {
            $id = input('post.id', 0);
            $pid = input('post.category', 0);
            $title = input('post.title');

            if(empty($id) || $id <= 0) {
                $this->resultData('$_221');
            }
            if(empty($title)) {
                $this->resultData('$_220');
            }

            $res = Db::name('news_category')->field('pid_fields')->where(['id' => $pid])->find();
            $pid_fields = $res ? $res['pid_fields'] : '0';
            $updateData = [
                'pid' => $pid,
                'title' => $title,
                'pid_fields' => $pid_fields . ',' . $id,
                'status' => input('post.status', 0)
            ];
            try{
                $update = Db::name('news_category')->where(['id'=>$id])->update($updateData);
                if($update === false) {
                    $this->resultData('$_101');
                } else {
                    $this->resultData('$_0');
                }
            } catch (\Exception $e) {
                $this->resultData('$_101');
            }
        }
    }

    public function delete() {
        $id = input('post.id', 0);
        if(empty($id)) {
            $this->resultData('$_221');
        }
        $updateRes1 = $updateRes2 = false;
        Db::transaction(function () use ($id, $updateRes1, $updateRes2) {
            $updateRes1 = Db::name('news_category')->where(['id'=>$id])->update(['status'=>0]);
            // 此分类下的文章都删除
            $updateRes2 = Db::name('news')->where(['category'=>$id])->update(['status'=>0]);
        });
        if($updateRes1 === false || $updateRes2 === false) {
            $this->resultData('$_101');
        } else {
            $this->resultData('$_0');
        }
    }
}